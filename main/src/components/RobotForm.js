import React, { Component } from 'react'
import RobotStore from '../stores/RobotStore'
import Robot from './Robot'



class RobotForm extends Component {
	constructor(){
		super()
		this.state = {
                mass: undefined,
                type: undefined,
                name: undefined

        }
        

        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        
        this.handleClick = () => {
            this.props.onAdd({
              name: this.state.name,
              type: this.state.type,
              mass: this.state.mass
            })
        }
    }

    render(){

        return <div>
              <input type="text" placeholder="name" name="name" onChange={this.handleChange} id="name"/>
            <input type="text" placeholder="type" name="type" onChange={this.handleChange} id="type"/>
            <input type="text" placeholder="mass" name="mass" onChange={this.handleChange} id="mass"/>
            <input type="button" value="add" onClick={this.handleClick} />
        </div>
    }
}

export default RobotForm